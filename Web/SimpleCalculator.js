function calculate(operator) {
	var number1, number2, operator, result;
	number1 = document.getElementById('n1').value;
	number1 = parseInt(number1);

	operator = document.getElementById('math').value;

	number2 = document.getElementById('n2').value;
	number2 = parseInt(number2);

	if (operator == "+") {
		result = number1 + number2;
	}
	else if (operator == "-") {
		result = number1 - number2;
	}
	else if (operator == "*") {
		result = number1 * number2;
	}
	else if (operator == "/") {
		if (number2 != 0) {
			result = number1 / number2;
        } else {
        	alert("Divide by zero error");
        }
	}
	else {
		alert ('You entered wrong math operator')
	}

	document.getElementById('out').innerHTML = result;
}