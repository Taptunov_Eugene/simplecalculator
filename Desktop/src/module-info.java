module SimpleCalculator {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    requires org.junit.jupiter.api;
    opens com.Calculator;
}