package com.Calculator;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label output;

    @FXML
    private TextField number2_field;

    @FXML
    private Button calculateButton;

    @FXML
    private TextField operator_field;

    @FXML
    private TextField number1_field;

    @FXML
    void handleCalculateButtonAction(ActionEvent event) {
        try {
            float number1 = Float.parseFloat(number1_field.getText());
            float number2 = Float.parseFloat(number2_field.getText());
            String action = operator_field.getText();
            float result = 0;

            switch (action) {
                case "+":
                    result = number1 + number2;
                    output.setText(Float.toString(sum(number1, number2)));
                    break;
                case "-":
                    result = number1 - number2;
                    output.setText(Float.toString(subtract(number1, number2)));
                    break;
                case "*":
                    result = number1 * number2;
                    output.setText(Float.toString(multiply(number1, number2)));
                    break;
                case "/":
                    if (number2 != 0) {
                        result = number1 / number2;
                        output.setText(Float.toString(division(number1, number2)));
                    } else {
                        output.setText("Division by zero");
                    }
                    break;
                default:
                    output.setText("Enter only: +, -, *, /");
                    break;
            }
        } catch (Exception e) {
            output.setText("Use only numbers");
        }
    }

    public static float sum(float num1, float num2) {
        return num1 + num2;
    }

    public static float subtract(float num1, float num2) {
        return num1 - num2;
    }

    public static float multiply(float num1, float num2) {
        return num1 * num2;
    }

    public static float division(float num1, float num2) {
        return num1 / num2;
    }
}
