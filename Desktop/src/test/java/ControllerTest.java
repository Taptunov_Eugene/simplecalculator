
import com.Calculator.Controller;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {

    @org.junit.jupiter.api.Test
    void sum() {
        int firstNumber = 13, secondNumber = 5;
        assertEquals(firstNumber + secondNumber,
                Controller.sum(firstNumber, secondNumber), 0.1);
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        int firstNumber = 13, secondNumber = 5;
        assertEquals(firstNumber - secondNumber,
                Controller.subtract(firstNumber, secondNumber), 0.1);
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        int firstNumber = 13, secondNumber = 5;
        assertEquals(firstNumber * secondNumber,
                Controller.multiply(firstNumber, secondNumber), 0.1);
    }

    @org.junit.jupiter.api.Test
    void division() {
        float firstNumber = 13, secondNumber = 5;
        assertEquals(firstNumber / secondNumber,
                Controller.division(firstNumber, secondNumber), 0.1);
    }
}