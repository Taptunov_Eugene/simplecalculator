package com.example.simplecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v) {
        EditText num1 = (EditText) findViewById(R.id.firstNumber);
        EditText num2 = (EditText) findViewById(R.id.secondNumber);
        float firstNumber = Float.parseFloat(num1.getText().toString());
        float secondNumber = Float.parseFloat(num2.getText().toString());
        EditText operator = (EditText) findViewById(R.id.operator);
        EditText result = (EditText) findViewById(R.id.result);
        String action = operator.getText().toString();
        float operationResult;
        switch (action) {
            case "+":
                operationResult = firstNumber + secondNumber;
                operator.setText(Float.toString(operationResult));
                break;
            case "-":
                operationResult = firstNumber - secondNumber;
                operator.setText(Float.toString(operationResult));
                break;
            case "*":
                operationResult = firstNumber * secondNumber;
                operator.setText(Float.toString(operationResult));
                break;
            case "/":
                if (secondNumber == 0) {
                    result.setText("Division by zero");
                } else {
                    operationResult = (float) firstNumber / secondNumber;
                    result.setText(Float.toString(operationResult));
                }
                break;
            default:
                result.setText("Enter the correct data");
        }
    }
}